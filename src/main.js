import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 导入字体图标
import './assets/fonts/iconfont.css'
// 导入全局样式表
import './assets/css/global.css'

import axios from 'axios'
// 配置请求的根路径
axios.defaults.baseURL = 'http://tsundere.icu:8889/api/private/v1'
// http://112.74.106.93:8889/api/private/v1
// https://www.lianghj.top:8888/api/private/v1/
// 'http://tsundere.icu:8889/api/private/v1

// 请求拦截器 为有权限的api正常调用成功  为请求头对象 添加token验证的 Authorization字段
axios.interceptors.request.use(config => {
  console.log(config)
  config.headers.Authorization = window.sessionStorage.getItem('token')
  // 在最后必须return config
  return config
})
Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.use(ElementUI)

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
